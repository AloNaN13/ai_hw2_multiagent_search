"""
Search Algos: MiniMax, AlphaBeta
"""
from utils import ALPHA_VALUE_INIT, BETA_VALUE_INIT, get_directions
import time


class SearchAlgos:
    def __init__(self, utility, succ, reverse, perform_move, goal=None):
        """The constructor for all the search algos.
        :param utility: The utility function.
        :param succ: The successor function.
        :param perform_move: The perform move function.
        :param goal: function that check if you are in a goal state.
        """
        self.utility = utility
        self.succ = succ
        self.reverse = reverse
        self.perform_move = perform_move
        self.goal = goal

    def search(self, state, depth, maximizing_player, time_limit):
        pass


class MiniMax(SearchAlgos):

    def search(self, state, depth, end_time, maximizing_player):
        """Start the MiniMax algorithm.
        :param state: The state to start from.
        :param depth: The maximum allowed depth for the algorithm.
        :param maximizing_player: Whether this is a max node (True) or a min node (False).
        :return: A tuple: (The min max algorithm value, The direction in case of max node or None in min mode)
        """
        player = state.Player.AGENT
        if not maximizing_player:
            player = state.Player.OPPONENT
        if self.goal(state, player) or depth == 0:
            return self.utility(state, player), None
        if time.time() + 0.1 >= end_time:  # 0.1 as the chosen epsilon
            return None, None
        if maximizing_player:
            current_max = float('-inf')
            current_best_direction = None
            for d in get_directions():
                next_state = self.succ(state, state.Player.AGENT, d)
                if next_state is None: # no side effects from succ function
                    continue
                score, direction = self.search(next_state, depth-1, end_time, False)
                self.reverse(state, state.Player.AGENT, d)
                if score is None:  # in case time expires
                    return None, None
                if score > current_max:
                    current_max = score
                    current_best_direction = d
            return current_max, current_best_direction
        else:
            current_min = float('inf')
            for d in get_directions():
                next_state = self.succ(state, state.Player.OPPONENT, d)
                if next_state is None:  # no side effects from succ function
                    continue
                score, direction = self.search(next_state, depth-1, end_time, True)
                self.reverse(state, state.Player.OPPONENT, d)
                if score is None:   # in case time expired
                    return None, None
                if score < current_min:
                    current_min = score
            return current_min, None


class AlphaBeta(SearchAlgos):

    def search(self, state, depth, end_time, maximizing_player, alpha=ALPHA_VALUE_INIT, beta=BETA_VALUE_INIT):
        """Start the AlphaBeta algorithm.
        :param state: The state to start from.
        :param depth: The maximum allowed depth for the algorithm.
        :param maximizing_player: Whether this is a max node (True) or a min node (False).
        :param alpha: alpha value
        :param beta: beta value
        :return: A tuple: (The min max algorithm value, The direction in case of max node or None in min mode)
        """
        player = state.Player.AGENT
        if not maximizing_player:
            player = state.Player.OPPONENT
        if self.goal(state, player) or depth == 0:
            return self.utility(state, player), None

        if time.time() + 0.1 >= end_time: # 0.1 as the chosen epsilon
            return None, None

        if maximizing_player:
            current_max = float('-inf')
            current_best_direction = None
            for d in get_directions():
                next_state = self.succ(state, state.Player.AGENT, d)
                if next_state is None: # no side effects from succ function
                    continue
                score, direction = self.search(next_state, depth-1, end_time, False, alpha, beta)
                self.reverse(state, state.Player.AGENT, d) # No need the change the alpha/beta back
                if score is None:  # in case time expires
                    return None, None
                if score > current_max:
                    current_max = score
                    current_best_direction = d
                if current_max > alpha:
                    alpha = current_max
                if current_max >= beta:
                    return float('inf'), current_best_direction
            return current_max, current_best_direction
        else:
            current_min = float('inf')
            for d in get_directions():
                next_state = self.succ(state, state.Player.OPPONENT, d)
                if next_state is None: # no side effects from succ function
                    continue
                score, direction = self.search(next_state, depth-1, end_time, True, alpha, beta)
                self.reverse(state, state.Player.OPPONENT, d) # No need the change the alpha/beta back
                if score is None:   # in case time expired
                    return None, None
                if score < current_min:
                    current_min = score
                if current_min < beta:
                    beta = current_min
                if current_min <= alpha:
                    return float('-inf'), None
            return current_min, None

        # should call this function with alpha=-inf, beta=inf

