"""
Player for the competition
"""
from players.AbstractPlayer import AbstractPlayer
import numpy as np
from utils import get_directions
from enum import Enum
from SearchAlgos import AlphaBeta
import time
import networkx as nx


def move_legal(board, pos, direction):
    old_i = pos[0]
    old_j = pos[1]
    new_i = direction[0] + old_i
    new_j = direction[1] + old_j
    if 0 <= new_i < len(board) and 0 <= new_j < len(board[0]) \
            and (board[new_i][new_j] not in [-1, 1, 2]):
        return True
    else:
        return False


def num_moves_available(board, pos):
    counter = 0
    for direction in get_directions():
        if move_legal(board, pos, direction):
            counter += 1
    return counter


class Player(AbstractPlayer):
    class State:

        class Player(Enum):
            #   AGENT_TURN = 'AGENT_TURN'
            #   OPPONENT_TURN = 'OPPONENT_TURN'
            AGENT = 'AGENT'
            OPPONENT = 'OPPONENT'

        def __init__(self, board, board_graph, scores, fruits_remaining_time, positions):
            self.board = board
            self.board_graph = board_graph
            self.scores = scores
            self.fruits_remaining_time = fruits_remaining_time
            self.positions = positions

    def __init__(self, game_time, penalty_score):
        AbstractPlayer.__init__(self, game_time,
                                penalty_score)  # keep the inheritance of the parent's (AbstractPlayer) __init__()
        self.initial_board = None
        self.search_algorithm = AlphaBeta(self.utility, self.succ_without_copies, self.reverse, self.perform_move,
                                          self.goal)
        self.current_state = None
        self.end_game_time = time.time() + game_time

    def set_game_params(self, board):
        self.initial_board = board
        player1_pos = np.where(board == 1)
        player2_pos = np.where(board == 2)
        positions = {self.State.Player.AGENT: tuple(ax[0] for ax in player1_pos),
                     self.State.Player.OPPONENT: tuple(ax[0] for ax in player2_pos)}
        short_edge = len(board)
        if (len(board[0])) < short_edge:
            short_edge = len(board[0])
        board_graph = self.graph_from_board(board)
        initial_board_copy = self.initial_board.copy()
        current_state = self.State(initial_board_copy, board_graph, {self.State.Player.AGENT: 0,
                                                                     self.State.Player.OPPONENT: 0},
                                   2*short_edge, positions)
        self.current_state = current_state

    def graph_from_board(self, board):
        board_graph = nx.DiGraph()
        not_blocked = np.argwhere(board != -1)
        not_blocked = list(map(tuple, not_blocked))
        for square in not_blocked:
            board_graph.add_node(square)
        for square in not_blocked:
            for d in get_directions():
                if move_legal(board, square, d):
                    board_graph.add_edge(square, (square[0] + d[0], square[1] + d[1]))
        return board_graph

    # Helper function for calculating the time of turn from the global_time
    def get_time_for_turn(self, state, time_left):
        '''
        # Naive formula for testing
        board = state.board
        num_of_available_squares = 0
        for i in range(len(board)):
            for j in range(len(board[0])):
                if board[i][j] not in [-1, 1, 2]:
                    num_of_available_squares += 1
        num_of_turns_upper_bound = round(num_of_available_squares/2)
        return time_left / num_of_turns_upper_bound
        '''

        # Trying to write the advanced algorithm for time calc
        # add an epsilon for the time it takes to calc num_reachable_squares
        reachable_agent = self.num_reachable(state, self.State.Player.AGENT)
        reachable_opponent = self.num_reachable(state, self.State.Player.OPPONENT)
        num_of_turns_upper_bound = (min(reachable_agent, reachable_opponent))
        if num_of_turns_upper_bound < 1:  # added 1 as the lower upper bound (also to never divide by zero)
            num_of_turns_upper_bound = 1
        return time_left / num_of_turns_upper_bound

    # returns first possible move in current state, or None if such does not exist.
    def default_move(self, state, player):
        for d in get_directions():
            if self.is_move_legal(state, player, d):
                return d
        return None

    def make_move(self, time_limit, players_score):
        """Make move with this Player.
        input:
            - time_limit: float, time limit for a single turn.
        output:
            - direction: tuple, specifying the Player's movement, chosen from self.directions
        """
        game_time_left = self.end_game_time - time.time()
        time_left = self.get_time_for_turn(self.current_state, game_time_left)
        end_time = time.time() + time_left
        depth = 1
        init_alpha = float('-inf')
        init_beta = float('inf')
        best_move = self.default_move(self.current_state, self.State.Player.AGENT)
        # assert (best_move is not None)  # if no moves are possible, agent shouldn't be given another turn
        # Should we take the time to make these few actions in consideration?
        iteration_time = 0
        while time_left > 0.2 and iteration_time <= time_left:
            start = time.time()
            _, new_best_move = self.search_algorithm.search(self.current_state,
                                                            depth, end_time, True,
                                                            init_alpha, init_beta)
            if new_best_move is None:
                # use SimplePlayer heuristic to determine a random legal move
                break
            best_move = new_best_move
            depth += 1
            end = time.time()
            iteration_time = start - end
            time_left -= iteration_time

        self.current_state = self.succ_without_copies(self.current_state, self.State.Player.AGENT, best_move)
        # assert (best_move is not None)
        return best_move

    def set_rival_move(self, pos):
        """Update your info, given the new position of the rival.
        input:
            - pos: tuple, the new position of the rival.
        No output is expected
        """
        old_opponent_position = self.current_state.positions[self.State.Player.OPPONENT]
        # assert self.current_state.board[old_opponent_position[0]][old_opponent_position[1]] == 2
        direction = tuple(np.subtract(pos, old_opponent_position))
        # assert direction in [(1, 0), (-1, 0), (0, 1), (0, -1)]
        # assert self.is_move_legal(self.current_state, self.State.Player.OPPONENT, direction)
        new_state = self.succ_without_copies(self.current_state, self.State.Player.OPPONENT, direction)
        self.current_state = new_state

    def update_fruits(self, fruits_on_board_dict):
        """Update your info on the current fruits on board (if needed).
        input:
            - fruits_on_board_dict: dict of {pos: value}
                                    where 'pos' is a tuple describing the fruit's position on board,
                                    'value' is the value of this fruit.
        No output is expected.
        """
        if self.current_state.fruits_remaining_time <= 0:
            # assert not np.argwhere((self.current_state.board != 1) & (self.current_state.board != 2) \
            #                       & (self.current_state.board != -1) & (self.current_state.board != 0)).any()
            # and not np.where(self.current_state.board not in [1, 2, -1])[1]
            return
        for position, value in fruits_on_board_dict.items():
            # assert self.current_state.board[position[0]][position[1]] not in [1, 2, -1]
            # assert self.initial_board[position[0]][position[1]] not in [1, 2, -1]
            if self.current_state.board[position[0]][position[1]] not in [1, 2, -1]:
                self.current_state.board[position[0]][position[1]] = value
            self.initial_board[position[0]][position[1]] = value


    ########## helper functions ##########

    def perform_move(self):
        pass

    # returns a tree of squares reachable from player
    def reachable_from_player(self, state, player, depth_limit=10):
        dfs_tree = nx.dfs_tree(state.board_graph, state.positions[player], depth_limit=depth_limit)
        return dfs_tree

    # returns the number of reachable squares on board from player's current location:
    def num_reachable(self, state, player):
        return self.reachable_from_player(state, player).number_of_nodes() - 1

    # returns a list of reachable fruits for player in current state
    def fruits_reachable_from_player(self, state, player):
        reachable_nodes = self.reachable_from_player(state, player)
        reachable_fruits = []
        for node in reachable_nodes:
            if (node[0], node[1]) == state.positions[player]:
                continue
            # assert state.board[node[0]][node[1]] not in [-1, 1, 2]
            if state.board[node[0], node[1]]:
                reachable_fruits.append(node)
        return reachable_fruits

    def man_dist(self, pos1, pos2):
        return abs(pos1[0] - pos2[0]) + abs(pos1[1] - pos2[1])

    def goal(self, state, player):
        return self.moves_available(state, player) == 0

    # returns whether moving in the specified direction is legal for player
    # kept for compatibility, use move_legal instead
    def is_move_legal(self, state, player, direction):
        return move_legal(state.board, state.positions[player], direction)

    # returns the number of legal moves for player in state
    def moves_available(self, state, player):
        return num_moves_available(state.board, state.positions[player])

    # returns the state obtained by performing the move specified by 'direction' while in current state, or None
    # in case this move is invalid.
    def succ(self, state, player, direction) -> State:
        if not self.is_move_legal(state, player, direction):
            return None

        old_i = state.positions[player][0]
        old_j = state.positions[player][1]
        new_i = direction[0] + old_i
        new_j = direction[1] + old_j
        new_board = np.copy(state.board)
        new_board[old_i][old_j] = -1

        new_board_graph = state.board_graph.copy()
        new_board_graph.remove_node((old_i, old_j))
        for d in get_directions():
            if new_board_graph.has_edge((new_i + d[0], new_j + d[1]), (new_i, new_j)):
                new_board_graph.remove_edge((new_i + d[0], new_j + d[1]), (new_i, new_j))

        new_positions = dict.copy(state.positions)
        new_positions[player] = (new_i, new_j)
        if player == self.State.Player.AGENT:
            new_board[new_i][new_j] = 1
        else:
            # assert (player == self.State.Player.OPPONENT)
            new_board[new_i][new_j] = 2

        new_scores = dict.copy(state.scores)
        new_scores[player] += state.board[new_i][new_j]
        if not num_moves_available(new_board, tuple((new_i, new_j))):  # game over for player, apply penalty
            new_scores[player] -= self.penalty_score

        new_fruits_remaining_time = state.fruits_remaining_time - 1
        if not new_fruits_remaining_time:
                fruit_indices = np.argwhere((new_board != 1) & (new_board != -1) & (new_board != 2))
                for p in fruit_indices:
                    x = p[0]
                    y = p[1]
                    new_board[x][y] = 0
        return self.State(new_board, new_board_graph, new_scores, new_fruits_remaining_time, new_positions)

    # reverses side effect of succ function, when player *got to* the current state using direction
    def reverse(self, state, player, direction) -> State:
        #   pass
        # update board
        current_i = state.positions[player][0]
        current_j = state.positions[player][1]
        previous_i = current_i - direction[0]
        previous_j = current_j - direction[1]
        refund = 0
        if self.goal(state, player):
            refund = 1

        if player == self.State.Player.AGENT:
            pass
            # assert state.board[current_i][current_j] == 1
        else:
            pass
            # assert player == self.State.Player.OPPONENT and state.board[current_i][current_j] == 2
        # update fruits_remaining_time
        state.fruits_remaining_time += 1
        if state.fruits_remaining_time > 0:
            state.board[current_i][current_j] = self.initial_board[current_i][current_j]
        else:
            state.board[current_i][current_j] = 0
        # assert move_legal(state.board, (previous_i, previous_j), direction)
        if player == self.State.Player.AGENT:
            state.board[previous_i][previous_j] = 1
        else:
            state.board[previous_i][previous_j] = 2
        # update positions
        state.positions[player] = (previous_i, previous_j)

        # update scores - if player lost, refund penalty
        state.scores[player] -= (state.board[current_i][current_j] - self.penalty_score * refund)

        # update fruits on board in case were deleted in previous turn
        if state.fruits_remaining_time == 1:
            fruit_indices = np.argwhere(
                (self.initial_board != 1) & (self.initial_board != -1) & (self.initial_board != 2))
            for p in fruit_indices:
                x = p[0]
                y = p[1]
                if state.board[x][y] not in [-1, 1, 2]:
                    # assert ((x == current_i and y == current_j) or state.board[x][y] == 0)
                    state.board[x][y] = self.initial_board[x][y]

        # update board_graph
        state.board_graph.add_node((previous_i, previous_j))
        for d in get_directions():
            # edges outsourced from previous square (no incoming edges since occupied)
            if move_legal(state.board, (previous_i, previous_j), d):
                state.board_graph.add_edge((previous_i, previous_j), (previous_i + d[0], previous_j + d[1]))
            d_rev = (-d[0], -d[1])
            # incoming edges to square from which player has withdrawn
            if self.in_board(state.board, (current_i + d[0], current_j + d[1])) and \
                    move_legal(state.board, (current_i + d[0], current_j + d[1]), d_rev):
                state.board_graph.add_edge((current_i + d[0], current_j + d[1]), (current_i, current_j))

        return state

    # update scores,positions, board, board_graph, fruits_remaining_time
    def succ_without_copies(self, state, player, direction) -> State:
        if not self.is_move_legal(state, player, direction):
            return None

        old_i = state.positions[player][0]
        old_j = state.positions[player][1]
        new_i = direction[0] + old_i
        new_j = direction[1] + old_j

        state.board[old_i][old_j] = -1

        state.board_graph.remove_node((old_i, old_j))
        for d in get_directions():
            if state.board_graph.has_edge((new_i + d[0], new_j + d[1]), (new_i, new_j)):
                state.board_graph.remove_edge((new_i + d[0], new_j + d[1]), (new_i, new_j))

        state.scores[player] += state.board[new_i][new_j]

        state.positions[player] = (new_i, new_j)
        if player == self.State.Player.AGENT:
            state.board[new_i][new_j] = 1
        else:
            # assert (player == self.State.Player.OPPONENT)
            state.board[new_i][new_j] = 2

        if not num_moves_available(state.board, tuple((new_i, new_j))):  # game over for player, apply penalty
            state.scores[player] -= self.penalty_score

        state.fruits_remaining_time -= 1
        if state.fruits_remaining_time == 0:
            fruit_indices = np.argwhere((state.board != 1) & (state.board != -1) & (state.board != 2))
            for p in fruit_indices:
                x = p[0]
                y = p[1]
                state.board[x][y] = 0
        return state

    def in_board(self, board, position):
        return 0 <= position[0] < len(board) and 0 <= position[1] < len(board[0])


    def simple_heuristic(self, state, player):
        num_available_moves = self.moves_available(state, player)
        if num_available_moves == 0:
            return -1
        else:
            return 4 - num_available_moves
        # utility for AGENT! player is used to determine who got blocked first.

    def utility(self, state, player):
        if self.goal(state, player):
            if player == self.State.Player.AGENT:
                return state.scores[self.State.Player.AGENT] - self.penalty_score
            else:
                return state.scores[self.State.Player.AGENT]
        else:
            return self.heuristic(state, player)

    def penalty_prob(self, state):
        pos_agent = state.positions[self.State.Player.AGENT]
        pos_opponent = state.positions[self.State.Player.OPPONENT]
        dist_agents = self.man_dist(pos_agent, pos_opponent)

        reachable_agent = self.num_reachable(state, self.State.Player.AGENT)
        reachable_opponent = self.num_reachable(state, self.State.Player.OPPONENT)
        reachable_ratio = reachable_opponent / (reachable_agent + reachable_opponent)

        if dist_agents <= 2:
            return reachable_ratio
        else:
            return (reachable_ratio + 1 - num_moves_available(state.board, pos_agent)/3)/2

    def fruits_heuristic(self, state):
        reachable_fruits = self.fruits_reachable_from_player(state, self.State.Player.AGENT)
        reachable_fruits_opponent = self.fruits_reachable_from_player(state, self.State.Player.OPPONENT)
        weighted_sum = 0
        total_dist = 0
        curr_loc = state.positions[self.State.Player.AGENT]
        curr_loc_op = state.positions[self.State.Player.OPPONENT]
        for fruit in reachable_fruits:
            val = state.board[fruit[0]][fruit[1]]
            md_ag = self.man_dist(fruit, curr_loc)
            # assert md_ag
            total_dist += 1/md_ag
            md_op = self.man_dist(fruit, curr_loc_op)
            if md_ag > (state.fruits_remaining_time/2):
                continue
            if fruit in reachable_fruits_opponent and md_op <= (state.fruits_remaining_time/2):
                weighted_sum += (md_op/(md_ag+md_op))*(val/md_ag)
            else:
                weighted_sum += val/md_ag
        if weighted_sum:
            # assert total_dist
            return weighted_sum/total_dist
        else:
            return 0


    def heuristic(self, state, player):
        scores_diff = state.scores[self.State.Player.AGENT] - state.scores[self.State.Player.OPPONENT]
        fruits_prospect = self.fruits_heuristic(state)
       # fruits_prospect = 0
        return scores_diff + fruits_prospect - self.penalty_score * self.penalty_prob(state)

    def reverse_pass(self, state, player, d):
        pass

